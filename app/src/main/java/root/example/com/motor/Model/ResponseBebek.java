package root.example.com.motor.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import root.example.com.motor.Model.itemResponse.BebekItem;

public class ResponseBebek{

	@SerializedName("bebek")
	private List<BebekItem> bebek;

	public void setBebek(List<BebekItem> bebek){
		this.bebek = bebek;
	}

	public List<BebekItem> getBebek(){
		return bebek;
	}

	@Override
 	public String toString(){
		return 
			"ResponseBebek{" + 
			"bebek = '" + bebek + '\'' + 
			"}";
		}
}