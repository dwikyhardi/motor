package root.example.com.motor.Model.itemResponse;

import com.google.gson.annotations.SerializedName;

public class NakedItem{

	@SerializedName("motor_type")
	private String motorType;

	@SerializedName("price")
	private int price;

	@SerializedName("id")
	private int id;

	@SerializedName("url_image")
	private String urlImage;

	@SerializedName("capacity")
	private String capacity;

	public void setMotorType(String motorType){
		this.motorType = motorType;
	}

	public String getMotorType(){
		return motorType;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUrlImage(String urlImage){
		this.urlImage = urlImage;
	}

	public String getUrlImage(){
		return urlImage;
	}

	public void setCapacity(String capacity){
		this.capacity = capacity;
	}

	public String getCapacity(){
		return capacity;
	}

	@Override
 	public String toString(){
		return 
			"NakedItem{" + 
			"motor_type = '" + motorType + '\'' + 
			",price = '" + price + '\'' + 
			",id = '" + id + '\'' + 
			",url_image = '" + urlImage + '\'' + 
			",capacity = '" + capacity + '\'' + 
			"}";
		}
}