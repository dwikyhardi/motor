package root.example.com.motor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import root.example.com.motor.Model.UserLogin;
import root.example.com.motor.REST.ApiInterface;
import root.example.com.motor.REST.Config;

public class MainActivity extends AppCompatActivity {

    final String TAG = "MainActivity";
    ImageView imgIcon;
    EditText Username, Password;
    Button btnLogin;
    Context context;
    SharedPreferences sharedPref;
    SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fabric.with(this, new Crashlytics());
        context = this;
        sharedPref = context.getSharedPreferences(
                getString(R.string.SharedPref), Context.MODE_PRIVATE);
        mEditor = sharedPref.edit();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "run() returned: " + sharedPref.getString(getResources().getString(R.string.SharedPref), ""));
                if (sharedPref.getString(getResources().getString(R.string.SharedPref), "").equals("Sudah Login")) {
                    startActivity(new Intent(MainActivity.this, Dashboard.class));
                    finish();
                }
            }
        }, 500);


        imgIcon = (ImageView) findViewById(R.id.imgIcon);
        Username = (EditText) findViewById(R.id.EtUsername);
        Password = (EditText) findViewById(R.id.EtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        Uri mUri = Uri.parse(getResources().getString(R.string.SplashScreen));
        Glide.with(this).load(mUri).into(imgIcon);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiInterface mApiInterface = Config.getClient().create(ApiInterface.class);
                UserLogin userLogin = new UserLogin(Username.getText().toString().trim(), Password.getText().toString().trim());
                Call<UserLogin> userLoginCall = mApiInterface.login(userLogin);
                userLoginCall.enqueue(new Callback<UserLogin>() {
                    @Override
                    public void onResponse(Call<UserLogin> call, Response<UserLogin> response) {
                        if (response.code() == 201) {
                            mEditor.putString(getResources().getString(R.string.SharedPref), "Sudah Login");
                            mEditor.commit();
                            Toast.makeText(MainActivity.this, "Success Login", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(MainActivity.this, Dashboard.class));
                            finish();
                        } else {
                            Toast.makeText(MainActivity.this, "Wrong Username or Password", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserLogin> call, Throwable t) {

                    }
                });
            }
        });
    }


}
