package root.example.com.motor.Tab;

import android.content.Intent;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import root.example.com.motor.Detail;
import root.example.com.motor.Model.ResponseBebek;
import root.example.com.motor.Model.itemResponse.BebekItem;
import root.example.com.motor.Model.itemResponse.MaticItem;
import root.example.com.motor.REST.ApiInterface;
import root.example.com.motor.REST.Config;

public class Bebek extends Parent {
    @Override
    public void getData() {
        ApiInterface mApiInterface = Config.getClient().create(ApiInterface.class);
        Call<ResponseBebek> mResponseCall = mApiInterface.getBebek();
        mResponseCall.enqueue(new Callback<ResponseBebek>() {
            @Override
            public void onResponse(Call<ResponseBebek> call, Response<ResponseBebek> response) {
                ResponseBebek mResponseBebek = response.body();

                int i = 0;
                while (i < mResponseBebek.getBebek().size()) {
                    mMaticItems.add(mResponseBebek.getBebek().get(i));
                    i++;
                }
                if (!mMaticItems.isEmpty()) {
                    initializeRecycleView(mMaticItems);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBebek> call, Throwable t) {

            }
        });
    }

    @Override
    public void showDialog(MaticItem maticItem) {
        if (maticItem instanceof BebekItem) {
            Intent mIntent = new Intent(getActivity(), Detail.class);
            Gson gsonBuilder = new Gson();
            String json = gsonBuilder.toJson(maticItem);
            mIntent.putExtra("motor", json);
            startActivity(mIntent);
        }
    }
}
