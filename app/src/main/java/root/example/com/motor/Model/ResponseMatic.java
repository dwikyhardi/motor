package root.example.com.motor.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import root.example.com.motor.Model.itemResponse.MaticItem;

public class ResponseMatic{

	@SerializedName("matic")
	private List<MaticItem> matic;

	public void setMatic(List<MaticItem> matic){
		this.matic = matic;
	}

	public List<MaticItem> getMatic(){
		return matic;
	}

	@Override
 	public String toString(){
		return 
			"ResponseMatic{" + 
			"matic = '" + matic + '\'' + 
			"}";
		}
}