package root.example.com.motor.Model.itemResponse;

import com.google.gson.annotations.SerializedName;

public class SportItemMI extends MaticItem {

    public SportItemMI(String motorType, int price, int id, String urlImage, String capacity , int gear) {
        this.motorType = motorType;
        this.price = price;
        this.id = id;
        this.urlImage = urlImage;
        this.capacity = capacity;
        this.gear = gear;
    }

    public SportItemMI(){

    }

    @SerializedName("gear")
    protected int gear;

    public int getGear() {
        return gear;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }
}