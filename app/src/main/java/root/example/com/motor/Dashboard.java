package root.example.com.motor;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

import root.example.com.motor.Adapter.ViewPagerAdapter;
//import root.example.com.motor.Tab.Bebek;
//import root.example.com.motor.Tab.Matic;
import root.example.com.motor.Tab.Bebek;
import root.example.com.motor.Tab.Parent;
import root.example.com.motor.Tab.Sport;
import root.example.com.motor.db.DatabaseModel;

public class Dashboard extends AppCompatActivity {

    private final String TAG = "Dashboard";
    private ViewPager viewPager;
    private TabLayout tabs;
    private ViewPagerAdapter mViewPagerAdapter;
    private TextView toolbar_text;
    private FloatingActionButton mButton;
    Context context;
    SharedPreferences sharedPref;
    SharedPreferences.Editor mEditor;

//    //fragments
//    Bebek mBebek;
//    /*Cross mCross;*/
//    Matic mMatic;
    /*Naked mNaked;*/
    Parent mSport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        context = this;
        sharedPref = context.getSharedPreferences(
                getString(R.string.SharedPref), Context.MODE_PRIVATE);
        mEditor = sharedPref.edit();

//        Configuration configuration = new Configuration.Builder(this)
//                .setDatabaseName("Motor.db")
//                .addModelClass(DatabaseModel.class)
//                .create();
//        ActiveAndroid.initialize(configuration);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Motor");
        toolbar_text = (TextView) toolbar.findViewById(R.id.title);
        toolbar_text.setText(getResources().getString(R.string.Sport));
        viewPager = findViewById(R.id.view_pager);
        setupViewPager(viewPager);
        tabs = findViewById(R.id.tablayout);
        mButton = (FloatingActionButton) findViewById(R.id.logout);

        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition(), false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.remove(getResources().getString(R.string.SharedPref));
                mEditor.commit();

                finish();
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        toolbar_text.setText(getResources().getString(R.string.Sport));
                        break;
                    case 1:
                        toolbar_text.setText(getResources().getString(R.string.Matic));
                        break;
                    case 2:
                        toolbar_text.setText(getResources().getString(R.string.Bebek));
                        break;
                    /*case 3:
                        toolbar_text.setText(getResources().getString(R.string.Naked));
                        break;
                    case 4:
                        toolbar_text.setText(getResources().getString(R.string.Cross));
                        break;*/

                }
                tabs.getTabAt(position).select();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        Parent mBebek = new Bebek();
        /*mCross = new Cross();*/
        Parent mMatic = new Parent();
        /*mNaked = new Naked();*/
        mSport = new Sport();

        mViewPagerAdapter.addFragment(mSport, getResources().getString(R.string.Sport));
        mViewPagerAdapter.addFragment(mMatic, "Matic");
        mViewPagerAdapter.addFragment(mBebek, "Bebek");
        /*mViewPagerAdapter.addFragment(mNaked, "Naked");
        mViewPagerAdapter.addFragment(mCross, "Cross");*/

        viewPager.setAdapter(mViewPagerAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}