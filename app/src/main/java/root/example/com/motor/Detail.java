package root.example.com.motor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import root.example.com.motor.Model.itemResponse.BebekItem;
import root.example.com.motor.Model.itemResponse.MaticItem;
import root.example.com.motor.Model.itemResponse.SportItemMI;

public class Detail extends AppCompatActivity {

    private final String TAG = "Detail";
    private String motorType, capacity, urlImage;
    private int price, gear;
    boolean kopling;
    private ImageView imgDetail;
    private TextView MotorType, EngineCapacity, Price, Gear, Kopling, toolbar_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar_text = (TextView) toolbar.findViewById(R.id.title);

        initView();
        Gson gsonBuilder = new Gson();
        String json = getIntent().getStringExtra("motor");
        Log.d(TAG, "onCreate() returned: " + json);
        MaticItem maticItem = null;
        if (json.contains("gear") && json.contains("kopling")) {
            BebekItem bebekItem = gsonBuilder.fromJson(json, BebekItem.class);
            kopling = bebekItem.isKopling();
            maticItem = bebekItem;
        } else if (json.contains("gear")) {
            SportItemMI sportItemMI = gsonBuilder.fromJson(json, SportItemMI.class);
            gear = sportItemMI.getGear();
            kopling = true;
            maticItem = sportItemMI;
        } else {//matic
            maticItem = gsonBuilder.fromJson(json, MaticItem.class);
        }

        motorType = maticItem.getMotorType();
        capacity = maticItem.getCapacity();
        urlImage = maticItem.getUrlImage();
        price = maticItem.getPrice();


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_text.setText(motorType);
        MotorType.setText(motorType);
        EngineCapacity.setText(capacity);
        Price.setText(String.valueOf(price));
        if (gear == 0) {
            Gear.setText("Matic");
        } else {
            Gear.setText(String.valueOf(gear));
        }
        if (kopling) {
            Kopling.setText("Ya");
        } else {
            Kopling.setText("Tidak");
        }
        Glide.with(this).load(urlImage).into(imgDetail);
    }

    private void initView() {
        imgDetail = (ImageView) findViewById(R.id.imgDetail);
        MotorType = (TextView) findViewById(R.id.motorType);
        EngineCapacity = (TextView) findViewById(R.id.engineCapacity);
        Price = (TextView) findViewById(R.id.price);
        Gear = (TextView) findViewById(R.id.gear);
        Kopling = (TextView) findViewById(R.id.kopling);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
