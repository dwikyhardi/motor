package root.example.com.motor.db;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Sport")
public class DatabaseModel extends Model {

    @Column(name = "idMotor")
    public int idMotor;

    @Column(name = "motorType")
    public String motorType;

    @Column(name = "price")
    public int price;

    @Column(name = "urlImage")
    public String urlImage;

    @Column(name = "capacity")
    public String capacity;

    public void setIdMotor(int idMotor) {
        this.idMotor = idMotor;
    }

    public void setMotorType(String motorType) {
        this.motorType = motorType;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }

    @Column(name = "gear")
    public int gear;

    public int getIdMotor() {
        return idMotor;
    }

    public String getMotorType() {
        return motorType;
    }

    public int getPrice() {
        return price;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public String getCapacity() {
        return capacity;
    }

    public int getGear() {
        return gear;
    }

    public DatabaseModel() {
        super();
    }


    public DatabaseModel(int id, String motorType, int price, String urlImage, String capacity, int gear) {
        super();
        this.idMotor = id;
        this.motorType = motorType;
        this.price = price;
        this.urlImage = urlImage;
        this.capacity = capacity;
        this.gear = gear;
    }

    public static List<Model> getAllData() {
        return new Select()
                .from(DatabaseModel.class)
                .execute();
    }
}
