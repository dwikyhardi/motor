package root.example.com.motor.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import root.example.com.motor.Model.itemResponse.MaticItem;
import root.example.com.motor.R;
import root.example.com.motor.holder.BebekHolder;

public class RecycleViewAdapterMaticParent extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RecycleViewAdapterMatic";
    private Context mContext;
    private ArrayList<MaticItem> maticItems;
    private RecycleViewAdapterBebekClickListener maticClickListener;
    private int VIEW_TYPE_ITEM = 0001;
    private int VIEW_TYPE_LOADING = 0002;
    private boolean isFooter;

    @Override
    public int getItemViewType(int position) {
        if (isFooter(position) && isFooter) {
            return VIEW_TYPE_LOADING;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    private boolean isFooter(int position) {
        return position==getItemCount()-1;
    }

    public RecycleViewAdapterMaticParent(ArrayList<MaticItem> maticItems,
                                         RecycleViewAdapterBebekClickListener maticClickListener,
                                         Context mContext, Boolean isFooter) {
        this.mContext = mContext;
        this.maticItems = maticItems;
        this.maticClickListener = maticClickListener;
        this.isFooter = isFooter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        MaticItem maticItem = maticItems.get(i);
        RecyclerView.ViewHolder holder = null;
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_view_style, viewGroup, false);
        if (i == VIEW_TYPE_ITEM) {
            return new BebekHolder(view);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.load_progress, viewGroup, false);
            return new ProgressViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        MaticItem maticItem = maticItems.get(i);
        if (viewHolder instanceof footerView){
            footerView mFooterView = (footerView) viewHolder;
            mFooterView.footer.setVisibility(View.VISIBLE);

        }
        else if (viewHolder instanceof BebekHolder) {
            BebekHolder bebekHolder = (BebekHolder) viewHolder;
            bebekHolder.getPrice().setText(maticItem.getPrice() + "");
            bebekHolder.getMotorType().setText(maticItem.getMotorType());
            bebekHolder.getEngineCapacity().setText(maticItem.getCapacity());
            Glide.with(mContext).load(maticItem.getUrlImage()).into(bebekHolder.getImage());
            bebekHolder.getItemView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (maticClickListener != null) {
                        maticClickListener.onItemClicked(i);
                    }
                }
            });
        }
    }
    public class footerView extends RecyclerView.ViewHolder {
        protected ProgressBar footer;
        public footerView(View v) {
            super(v);
            footer = (ProgressBar) v.findViewById(R.id.progressbar);
        }
    }

    @Override
    public int getItemCount() {
        return maticItems.size();
    }

    public interface RecycleViewAdapterBebekClickListener {
        void onItemClicked(int position);

    }

    class ProgressViewHolder extends CustomViewHolder {
        public ProgressViewHolder(View itemView) {
            super(itemView);
        }
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        public CustomViewHolder(View itemView) {
            super(itemView);
        }
    }
}
