package root.example.com.motor.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static io.fabric.sdk.android.Fabric.TAG;

public class SQLiteDataHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Motor";
    public static final int DATABASE_VERSION = 1;
    public String TABLE_NAME = "Sport";

    //Field
    public String id = "id";
    public String motorType = "motorType";
    public String price = "price";
    public String urlImage = "urlImage";
    public String capacity = "capacity";
    public String gear = "gear";

    public SQLiteDataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                id + "INTEGER PRIMARY KEY ," +
                motorType + "TEXT ," +
                price + "INTEGER ," +
                urlImage + "TEXT ," +
                capacity + "TEXT ," +
                gear + "INTEGER" +
                ")";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
