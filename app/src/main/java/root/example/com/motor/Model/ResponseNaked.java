package root.example.com.motor.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import root.example.com.motor.Model.itemResponse.NakedItem;


public class ResponseNaked{

	@SerializedName("naked")
	private List<NakedItem> naked;

	public void setNaked(List<NakedItem> naked){
		this.naked = naked;
	}

	public List<NakedItem> getNaked(){
		return naked;
	}

	@Override
 	public String toString(){
		return 
			"ResponseNaked{" + 
			"naked = '" + naked + '\'' + 
			"}";
		}
}