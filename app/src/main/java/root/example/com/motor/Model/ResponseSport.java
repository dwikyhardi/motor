package root.example.com.motor.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import root.example.com.motor.Model.itemResponse.SportItemMI;

public class ResponseSport {

	@SerializedName("sport")
	private List<SportItemMI> sport;

	public void setSport(List<SportItemMI> sport){
		this.sport = sport;
	}

	public List<SportItemMI> getSport(){
		return sport;
	}

	@Override
 	public String toString(){
		return 
			"ResponseSport{" +
			"sport = '" + sport + '\'' + 
			"}";
		}
}