package root.example.com.motor.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import root.example.com.motor.R;

public class BebekHolder extends RecyclerView.ViewHolder  {


    TextView motorType, engineCapacity, price;
    ImageView image;
    private View itemView;

    public BebekHolder(View itemView) {
        super(itemView);
        motorType = (TextView) itemView.findViewById(R.id.motorType);
        engineCapacity = (TextView) itemView.findViewById(R.id.engineCapacity);
        price = (TextView) itemView.findViewById(R.id.price);
        image = (ImageView) itemView.findViewById(R.id.image);

        this.setItemView(itemView);
    }

    public TextView getMotorType() {
        return motorType;
    }

    public void setMotorType(TextView motorType) {
        this.motorType = motorType;
    }

    public TextView getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(TextView engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public TextView getPrice() {
        return price;
    }

    public void setPrice(TextView price) {
        this.price = price;
    }

    public ImageView getImage() {
        return image;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }


    public View getItemView() {
        return itemView;
    }

    public void setItemView(View itemView) {
        this.itemView = itemView;
    }
}