package root.example.com.motor.Model.itemResponse;


import com.google.gson.annotations.SerializedName;

public class MaticItem {

    @SerializedName("motor_type")
    protected String motorType;

    @SerializedName("price")
    protected int price;

    @SerializedName("id")
    protected int id;

    @SerializedName("url_image")
    protected String urlImage;

    @SerializedName("mesin_capacity")
    protected String capacity;

    public void setMotorType(String motorType) {
        this.motorType = motorType;
    }

    public String getMotorType() {
        return motorType;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getCapacity() {
        return capacity;
    }

    @Override
    public String toString() {
        return
                "MaticItem{" +
                        "motor_type = '" + motorType + '\'' +
                        ",price = '" + price + '\'' +
                        ",id = '" + id + '\'' +
                        ",url_image = '" + urlImage + '\'' +
                        ",capacity = '" + capacity + '\'' +
                        "}";
    }
}