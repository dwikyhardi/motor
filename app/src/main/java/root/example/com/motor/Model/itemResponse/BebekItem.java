package root.example.com.motor.Model.itemResponse;

import com.google.gson.annotations.SerializedName;

public class BebekItem extends SportItemMI {

    @SerializedName("kopling")
    private boolean kopling;


    public void setKopling(boolean kopling) {
        this.kopling = kopling;
    }

    public boolean isKopling() {
        return kopling;
    }


}