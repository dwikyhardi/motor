package root.example.com.motor.Tab;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.activeandroid.util.SQLiteUtils;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import root.example.com.motor.Detail;
import root.example.com.motor.Model.ResponseSport;
import root.example.com.motor.Model.itemResponse.MaticItem;
import root.example.com.motor.Model.itemResponse.SportItemMI;
import root.example.com.motor.REST.ApiInterface;
import root.example.com.motor.REST.Config;
import root.example.com.motor.db.DatabaseModel;
import root.example.com.motor.db.SQLiteDataHelper;

public class Sport extends Parent {
    @Override
    public void getData() {
        final SQLiteDataHelper sqLiteDataHelper = new SQLiteDataHelper(getActivity());
        SQLiteDatabase readableDatabase = sqLiteDataHelper.getReadableDatabase();
        final SQLiteDatabase writableDatabase = sqLiteDataHelper.getWritableDatabase();
        Cursor getFromDB = readableDatabase.rawQuery("SELECT * FROM " + sqLiteDataHelper.TABLE_NAME, null);
//        final DatabaseModel databaseModel = new DatabaseModel();

        if (getFromDB.getCount() <= 0) {
            Log.d(TAG, "getData: From API");
            ApiInterface mApiInterface = Config.getClient().create(ApiInterface.class);
            Call<ResponseSport> mResponseCall = mApiInterface.getSport();
            mResponseCall.enqueue(new Callback<ResponseSport>() {
                @Override
                public void onResponse(Call<ResponseSport> call, retrofit2.Response<ResponseSport> response) {
                    ResponseSport mResponseSport1 = response.body();
                    int i = 0;
                    while (i < mResponseSport1.getSport().size()) {
                        mMaticItems.add(mResponseSport1.getSport().get(i));
                        SportItemMI sportItemMI = mResponseSport1.getSport().get(i);
                        String INSERT = "INSERT INTO " + sqLiteDataHelper.TABLE_NAME + " VALUES(" +
                                sportItemMI.getId() + ",'" +
                                sportItemMI.getMotorType() + "'," +
                                sportItemMI.getPrice() + ",'" +
                                sportItemMI.getUrlImage() + "','" +
                                sportItemMI.getCapacity() + "'," +
                                sportItemMI.getGear() + ")";
                        writableDatabase.execSQL(INSERT);

//                        databaseModel.idMotor = sportItemMI.getId();
//                        databaseModel.motorType = sportItemMI.getMotorType();
//                        databaseModel.price = sportItemMI.getPrice();
//                        databaseModel.urlImage = sportItemMI.getUrlImage();
//                        databaseModel.capacity = sportItemMI.getCapacity();
//                        databaseModel.gear = sportItemMI.getGear();
//                        databaseModel.save();

//                        Log.d(TAG, "onResponse() returned: " + databaseModel.getAllData().size());
                        i++;
                    }
                    if (!mMaticItems.isEmpty()) {
                        initializeRecycleView(mMaticItems);
                        swipeRefreshLayout.setRefreshing(false);
//                        Log.d(TAG, "onResponse() returned: " + databaseModel.getAllData());
//                        Log.d(TAG, "onResponse() returned: " + databaseModel.motorType);
//                        Log.d(TAG, "onResponse() returned: " + databaseModel.capacity);
//                        Log.d(TAG, "onResponse() returned: " + databaseModel.gear);
//                        Log.d(TAG, "onResponse() returned: " + databaseModel.price);
//                        Log.d(TAG, "onResponse() returned: " + databaseModel.urlImage);
                    }
                }

                @Override
                public void onFailure(Call<ResponseSport> call, Throwable t) {

                }
            });
        } else {
            Log.d(TAG, "getData: masuk Else");
//            Log.d(TAG, "getData() returned: " + databaseModel.getAllData().size());
//            int i = 0;
//            while (i < databaseModel.getAllData().size()) {
//                int id = databaseModel.idMotor;
//                String MotorType = databaseModel.motorType;
//                int Price = databaseModel.price;
//                String urlImage = databaseModel.urlImage;
//                String capacity = databaseModel.capacity;
//                int gear = databaseModel.gear;
//
//                mMaticItems.add(new SportItemMI(MotorType, Price, id, urlImage, capacity, gear));
//                Log.d(TAG, "getData() returned: " + mMaticItems.size());
//                i++;
//            }
//            initializeRecycleView(mMaticItems);

        Log.d(TAG, "getData: From Database");
        if (getFromDB.moveToFirst()) {
            int i = 0;
            do {
                int id = getFromDB.getInt(0);
                String MotorType = getFromDB.getString(1);
                int Price = getFromDB.getInt(2);
                String urlImage = getFromDB.getString(3);
                String capacity = getFromDB.getString(4);
                int gear = getFromDB.getInt(5);

                mMaticItems.add(new SportItemMI(MotorType, Price, id, urlImage, capacity, gear));
            } while (getFromDB.moveToNext());
            initializeRecycleView(mMaticItems);
            swipeRefreshLayout.setRefreshing(false);
        }
        }
    }


    public void showDialog(MaticItem sportItemMI) {
        if (sportItemMI instanceof SportItemMI) {
            Intent mIntent = new Intent(getActivity(), Detail.class);
            Gson gsonBuilder = new Gson();
            String json = gsonBuilder.toJson(sportItemMI);
            mIntent.putExtra("motor", json);
            startActivity(mIntent);
        }
    }
}
