package root.example.com.motor.REST;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import root.example.com.motor.Model.ResponseBebek;
import root.example.com.motor.Model.ResponseMatic;
import root.example.com.motor.Model.ResponseNaked;
import root.example.com.motor.Model.ResponseSport;
import root.example.com.motor.Model.UserLogin;

public interface ApiInterface {

    @GET("SPORT")
    Call<ResponseSport> getSport();

    @GET("matic")
    Call<ResponseMatic> getMatic();

    @GET("bebek")
    Call<ResponseBebek> getBebek();

    @POST("login")
    Call<UserLogin> login(@Body UserLogin userLogin);
}
