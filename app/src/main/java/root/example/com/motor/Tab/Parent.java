package root.example.com.motor.Tab;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import root.example.com.motor.Adapter.RecycleViewAdapterMaticParent;
import root.example.com.motor.Detail;
import root.example.com.motor.Model.ResponseMatic;
import root.example.com.motor.Model.itemResponse.MaticItem;
import root.example.com.motor.R;
import root.example.com.motor.REST.ApiInterface;
import root.example.com.motor.REST.Config;
import root.example.com.motor.db.SQLiteDataHelper;

public class Parent extends Fragment implements RecycleViewAdapterMaticParent.RecycleViewAdapterBebekClickListener {

    public final String TAG = "Parent";
    private RecycleViewAdapterMaticParent mViewAdapterSport;
    private RecyclerView RvSport;
    protected ArrayList<MaticItem> mMaticItems;
    Context mContext;
    SharedPreferences sharedPref;
    SharedPreferences.Editor mEditor;
    protected SwipeRefreshLayout swipeRefreshLayout;
    private Boolean isLoading = false;
    private Parcelable recyclerViewState;

    public Parent() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sport, container, false);
        RvSport = (RecyclerView) view.findViewById(R.id.RvSport);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        final SQLiteDataHelper sqLiteDataHelper = new SQLiteDataHelper(getActivity());
        final SQLiteDatabase writableDatabase = sqLiteDataHelper.getWritableDatabase();
        mContext = getActivity();
        sharedPref = mContext.getSharedPreferences(
                getString(R.string.SharedPrefMotorMatic), Context.MODE_PRIVATE);
        mEditor = sharedPref.edit();
        mMaticItems = new ArrayList<>();
        mMaticItems.clear();
        getData();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mMaticItems.clear();
                mEditor.clear();
                mEditor.apply();
                writableDatabase.rawQuery("DELETE FROM " + sqLiteDataHelper.TABLE_NAME, null);
                getData();
            }
        });
        initScrollListener();
        return view;
    }

    public void getData() {
        final Gson gsonBuilder = new Gson();
        if (sharedPref.getAll().isEmpty()) {
            Log.d(TAG, "getData: From API");
            ApiInterface mApiInterface = Config.getClient().create(ApiInterface.class);
            Call<ResponseMatic> mResponseCall = mApiInterface.getMatic();
            mResponseCall.enqueue(new Callback<ResponseMatic>() {
                @Override
                public void onResponse(Call<ResponseMatic> call, retrofit2.Response<ResponseMatic> response) {
                    ResponseMatic mResponse1 = response.body();
                    String json = gsonBuilder.toJson(mResponse1);
                    mEditor.putString("Matic", json);
                    mEditor.commit();
                    int i = 0;
                    while (i < mResponse1.getMatic().size()) {
                        mMaticItems.add(mResponse1.getMatic().get(i));
                        i++;
                    }

                    if (!mMaticItems.isEmpty()) {
                        initializeRecycleView(mMaticItems);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }

                @Override
                public void onFailure(Call<ResponseMatic> call, Throwable t) {

                }
            });
        } else {
            Log.d(TAG, "getData: From SharedPreference");
            String json = sharedPref.getString("Matic", "");
            String jsonArray = json.replace("{\"matic\":", "");
            StringBuffer stringBuffer = new StringBuffer(jsonArray);
            stringBuffer.replace(jsonArray.lastIndexOf("}"), jsonArray.lastIndexOf("}") + 1, "");
            JsonParser jsonParser = new JsonParser();
            JsonArray jsonArray1 = jsonParser.parse(stringBuffer.toString()).getAsJsonArray();
            int i = 0;
            while (i < jsonArray1.size()) {
                MaticItem maticItem = gsonBuilder.fromJson(jsonArray1.get(i), MaticItem.class);
                mMaticItems.add(maticItem);
                i++;
            }
            initializeRecycleView(mMaticItems);
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    protected void initializeRecycleView(ArrayList<MaticItem> mSportItems) {
        mViewAdapterSport = new RecycleViewAdapterMaticParent(mSportItems, this, getActivity(),true);
        RvSport.setLayoutManager(new LinearLayoutManager(getActivity()));
        RvSport.setHasFixedSize(false);
        RvSport.getLayoutManager().onRestoreInstanceState(recyclerViewState);
        RvSport.setAdapter(mViewAdapterSport);
    }

    public void showDialog(MaticItem maticItem) {
        Intent mIntent = new Intent(getActivity(), Detail.class);
        Gson gsonBuilder = new Gson();
        String json = gsonBuilder.toJson(maticItem);
        mIntent.putExtra("motor", json);
        startActivity(mIntent);
    }

    @Override
    public void onItemClicked(int position) {
        showDialog(mMaticItems.get(position));
    }

    private void initScrollListener() {
        RvSport.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == mMaticItems.size() - 1) {
                        if (linearLayoutManager.findLastCompletelyVisibleItemPosition() <= 75) {
                            recyclerViewState = RvSport.getLayoutManager().onSaveInstanceState();
                            isLoading = true;
                            loadMore();
                        } else {
                            Toast.makeText(mContext, "Max", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

    private void loadMore() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getData();
                isLoading = false;
            }
        }, 500);
    }
}